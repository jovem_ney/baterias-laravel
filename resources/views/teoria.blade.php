<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Baterias</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../css/estilos.css">
</head>
<body>
    <header>
        <h1>Baterias</h1>
    </header>
        
    <nav>
        <ul>
            <li><a href="../index.html">Voltar para página principal</a></li>
        </ul>
    </nav>

    <main>
        <h1>Teoria</h1>
        <hr>
        <h2>Descrição das pilhas:</h2>
        <p>
            As pilha são constituídas de .......
        </p>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lacus elit, condimentum at gravida eu, elementum in massa. Fusce at dapibus sem. Donec in hendrerit metus, vitae rutrum nulla. Nulla maximus, nibh vitae vulputate dignissim, purus libero euismod orci, non placerat lorem orci nec massa. Sed at quam eu mi maximus auctor. Etiam tincidunt massa enim, et fermentum nisl vulputate efficitur. Maecenas tristique diam eget maximus congue.
        </p>

        <img class="bateria" src="../imgs/bateria9V.jpeg" alt="bateria">

        <p>
            Etiam efficitur malesuada nulla ut rutrum. Aliquam pharetra eget augue at rhoncus. In purus diam, fringilla in enim mattis, ornare vestibulum risus. Donec condimentum finibus purus non aliquam. Curabitur sodales odio id magna venenatis, vel tristique justo congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut quis magna ipsum. Vivamus pharetra sollicitudin maximus. Nullam congue risus leo, consectetur sollicitudin justo laoreet ut. Praesent congue feugiat turpis id efficitur. Quisque sed urna tincidunt mi auctor cursus. Morbi tincidunt rutrum ultricies. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam erat volutpat. Fusce fringilla mollis arcu, nec commodo erat pharetra ac.
        </p>
    </main>
</body>
</html>